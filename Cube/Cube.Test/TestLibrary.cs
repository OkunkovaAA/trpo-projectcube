using NUnit.Framework;
using System;

namespace Cube.Test
{
    public class Tests
    {
        public double Rib = 7;

        [Test]
        public void DiagonalCube()
        {
            double ExpectedResult = 12.12;
            double ObtainedResult = new Cube.Library.Parameters().DiagonalCube(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void DiagonalCubeError()
        {
            double RibError = 0;
            Assert.Throws<ArgumentException>(() => new Cube.Library.Parameters().DiagonalCube(RibError));
        }

        [Test]
        public void DiagonalEdgeCube()
        {
            double ExpectedResult = 9.90;
            double ObtainedResult = new Cube.Library.Parameters().DiagonalEdgeCube(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void VolumeCube()
        {
            double ExpectedResult = 343;
            double ObtainedResult = new Cube.Library.Parameters().VolumeCube(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void SurfaceAreaCube()
        {
            double ExpectedResult = 294;
            double ObtainedResult = new Cube.Library.Parameters().SurfaceAreaCube(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void PerimeterCube()
        {
            double ExpectedResult = 84;
            double ObtainedResult = new Cube.Library.Parameters().PerimeterCube(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void RadiusInscribedSphere()
        {
            double ExpectedResult = 3.5;
            double ObtainedResult = new Cube.Library.Parameters().RadiusInscribedSphere(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void VolumeInscribedSphere()
        {
            double ExpectedResult = 179.60;
            double ObtainedResult = new Cube.Library.Parameters().VolumeInscribedSphere(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void RadiusDescribedSphere()
        {
            double ExpectedResult = 6.06;
            double ObtainedResult = new Cube.Library.Parameters().RadiusDescribedSphere(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }

        [Test]
        public void VolumeDescribedSphere()
        {
            double ExpectedResult = 933.20;
            double ObtainedResult = new Cube.Library.Parameters().VolumeDescribedSphere(Rib);
            Assert.AreEqual(ExpectedResult, ObtainedResult, 0.01);
        }
    }
}