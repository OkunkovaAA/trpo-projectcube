﻿using System;

namespace Cube.Library
{
    public class Parameters
    {
        //Проверка введенного пользователем длины ребра
        public void CheckRib(double Rib)
        {
            if (Rib < 0 || Rib > 500)
                throw new ArgumentException("Ребро куба не может быть меньше нуля и предел значения равен 500!");
        }

        //Диагональ куба
        public double DiagonalCube(double Rib)
        {
            CheckRib (Rib);
            return Rib * Math.Sqrt(3);
        }

        //Диагональ грани куба
        public double DiagonalEdgeCube(double Rib)
        {
            CheckRib(Rib);
            return Rib * Math.Sqrt(2);
        }

        //Объём куба
        public double VolumeCube(double Rib)
        {
            CheckRib(Rib);
            return Rib * Rib * Rib;
        }

        //Площадь поверхности куба
        public double SurfaceAreaCube(double Rib)
        {
            CheckRib(Rib);
            return 6 * Rib * Rib;
        }

        //Периметр куба
        public double PerimeterCube(double Rib)
        {
            CheckRib(Rib);
            return 12 * Rib;
        }

        //Радиус вписанной сферы
        public double RadiusInscribedSphere(double Rib)
        {
            CheckRib(Rib);
            return Rib / 2;
        }

        //Объём вписанной сферы
        public double VolumeInscribedSphere(double Rib)
        {
            CheckRib(Rib);
            return Math.PI * Rib * Rib * Rib / 6;
        }

        //Радиус описанной сферы
        public double RadiusDescribedSphere(double Rib)
        {
            CheckRib(Rib);
            return Rib * Math.Sqrt(3) / 2;
        }

        //Объём описанной сферы
        public double VolumeDescribedSphere(double Rib)
        {
            CheckRib (Rib);
            return Math.PI * Rib * Rib * Rib * Math.Sqrt(3) / 2;
        }
    }
}
