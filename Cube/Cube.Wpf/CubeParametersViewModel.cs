﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Cube.Wpf
{
    class CubeParametersViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string str = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(str));
        }

        private double _Rib;
        public CubeParametersViewModel()
        {
            _Rib = 7.7;
        }

        public double Rib
        {
            get { return _Rib; }

            set
            {
                _Rib = value;
                OnPropertyChanged("Rib");
                OnPropertyChanged("DiagonalCube");
                OnPropertyChanged("DiagonalEdgeCube");
                OnPropertyChanged("VolumeCube");
                OnPropertyChanged("SurfaceAreaCube");
                OnPropertyChanged("PerimeterCube");
                OnPropertyChanged("RadiusInscribedSphere");
                OnPropertyChanged("VolumeInscribedSphere");
                OnPropertyChanged("RadiusDescribedSphere");
                OnPropertyChanged("VolumeDescribedSphere");
            }
        }
        public double DiagonalCube
        {
            get { return new Cube.Library.Parameters().DiagonalCube(_Rib); }
        }

        public double DiagonalEdgeCube
        {
            get { return new Cube.Library.Parameters().DiagonalEdgeCube(_Rib); }
        }

        public double VolumeCube
        {
            get { return new Cube.Library.Parameters().VolumeCube(_Rib); }
        }

        public double SurfaceAreaCube
        {
            get { return new Cube.Library.Parameters().SurfaceAreaCube(_Rib); }
        }

        public double PerimeterCube
        {
            get { return new Cube.Library.Parameters().PerimeterCube(_Rib); }
        }

        public double RadiusInscribedSphere
        {
            get { return new Cube.Library.Parameters().RadiusInscribedSphere(_Rib); }
        }

        public double VolumeInscribedSphere
        {
            get { return new Cube.Library.Parameters().VolumeInscribedSphere(_Rib); }
        }

        public double RadiusDescribedSphere
        {
            get { return new Cube.Library.Parameters().RadiusDescribedSphere(_Rib); }
        }

        public double VolumeDescribedSphere
        {
            get { return new Cube.Library.Parameters().VolumeDescribedSphere(_Rib); }
        }

    }
}
