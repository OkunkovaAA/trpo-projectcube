﻿using System;
using System.Windows;
using System.Windows.Input;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace Cube.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new CubeParametersViewModel();
        }

        private void Instruction_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("Справка по использованию приложения:\n\n1) Введите в текстовое поле размер ребра куба. Десятичные значения вводить через точку\n2) Получите значения всех основных параметров куба\n3) При необходимости, сохраните все данные на свой ПК в виде изображения (в левой нижней части окна приложения)\n\nБлагодарим, что выбрали приложение \"Cube\"!", "Справочная информация");
        }

        private void SaveImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (TextBoxPathSave.Text[TextBoxPathSave.Text.Length - 1] == Convert.ToChar("\\"))
                {
                    TextBoxPathSave.Text = TextBoxPathSave.Text.TrimEnd(Convert.ToChar("\\")) + "\\";
                }
                else if (TextBoxPathSave.Text[TextBoxPathSave.Text.Length - 1] != Convert.ToChar("\\"))
                {
                    TextBoxPathSave.Text = TextBoxPathSave.Text + "\\";
                }

                RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap(1280, 720, 0, 0, PixelFormats.Pbgra32);
                renderTargetBitmap.Render(CubeParameters);
                PngBitmapEncoder pngImage = new PngBitmapEncoder();
                pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                using (Stream fileStream = File.Create(TextBoxPathSave.Text + "ПараметрыКуба.png"))
                {
                    pngImage.Save(fileStream);
                }
                MessageBox.Show("Результаты сохранёны, путь: " + TextBoxPathSave.Text + "ПараметрыКуба.png", "Сохранение завершено");
            }
            catch
            {
                MessageBox.Show("Не удалось определить указанный путь!", "Сохранение не завершено");
            }
        }

        private void TextBoxRib_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            var value = (TextBox)sender;
            if (value.Text != "")
            {
                try
                {
                    for (int i = 0; i < value.Text.Length; i++)
                    {
                        //Для возможности преобразовать строку в double
                        string number = "";
                        for (int j = 0; j < TextBoxRib.Text.Length; j++)
                        {
                            if (TextBoxRib.Text[j] == Convert.ToChar("."))
                                number = number + ",";
                            else
                                number = number + TextBoxRib.Text[j];
                        }
                        //Проверка на корректность ввода
                        if ((!Char.IsNumber(value.Text[i]) && value.Text[i] != Convert.ToChar(".")) || Convert.ToDouble(number) > 500)
                        {
                            MessageBox.Show("Размер ребра должен быть больше нуля и меньше 500, например \"7.7\"", "Неверные данные");
                            TextBoxRib.Text = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void TextBoxPathSave_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TextBoxPathSave.Text == "Введите путь, куда сохранить")
                TextBoxPathSave.Text = "";
        }

        private void TextBoxPathSave_LostFocus(object sender, RoutedEventArgs e)
        {
            if (TextBoxPathSave.Text == "")
                TextBoxPathSave.Text = "Введите путь, куда сохранить";
        }
    }
}
